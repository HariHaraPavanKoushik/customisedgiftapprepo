package com.org.customisedgifts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.customisedgifts.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	User findByUserId(long userId);

}
