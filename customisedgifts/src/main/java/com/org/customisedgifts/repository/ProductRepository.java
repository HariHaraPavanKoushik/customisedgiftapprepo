package com.org.customisedgifts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.customisedgifts.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

	Product findByProductId(long productId);

}
