package com.org.customisedgifts.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.customisedgifts.entity.Cart;

@Repository
@Transactional
public interface CartRepository extends JpaRepository<Cart, Long> {

	@Query("SELECT entity.status FROM Cart entity WHERE entity.cartId=?1 ")
	Boolean fetchbyCartStatus(long cardId);

	@Modifying
	@Query("UPDATE Cart entity SET entity.status = ?2 where entity.cartId=?1")
	void updateStatus(long cartId, boolean status);

	Cart findByCartId(long cartId);

	@Query("Select entity.status from Cart entity where entity.cartId=?1")
	boolean fetchStatus(long cartId);

}
