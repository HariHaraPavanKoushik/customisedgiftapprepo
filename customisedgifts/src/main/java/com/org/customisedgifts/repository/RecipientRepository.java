package com.org.customisedgifts.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.customisedgifts.entity.Recipient;

@Repository
public interface RecipientRepository extends JpaRepository<Recipient, Long> {

	@Query("SELECT entity FROM Recipient entity WHERE entity.recipientId=?1")
	Recipient fetchById(long recipientId);

	List<Recipient> findByCartCartId(long cartId);

	String findByMessage(long cartId);

}
