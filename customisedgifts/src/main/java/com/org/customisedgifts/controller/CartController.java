package com.org.customisedgifts.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.customisedgifts.dto.AddCartDto;
import com.org.customisedgifts.dto.AddCartResponseDto;
import com.org.customisedgifts.exception.CustomException;
import com.org.customisedgifts.exception.NotCorporateType;
import com.org.customisedgifts.exception.NotPersonalType;
import com.org.customisedgifts.exception.ProductNotFound;
import com.org.customisedgifts.exception.QuantityNotAvailable;
import com.org.customisedgifts.service.CartService;

@RestController
@CrossOrigin(origins = "*",allowedHeaders = "*")
/**
 * 
 * @author Koushik
 * @since 01-04-2020
 *
 */
public class CartController {
	
	@Autowired
	CartService cartService;
	
	/**
	 * In this functionality we will add the details to the cart
	 * @param addCartDto
	 * @return
	 * @throws CustomException		-for user not found exception
	 * @throws ProductNotFound		-for product not found exception
	 * @throws QuantityNotAvailable	-for quantity not available 
	 * @throws NotPersonalType		-for product type checking not personal
	 * @throws NotCorporateType		-for product type checking not corporate
	 * 
	 */
	@PostMapping("/cart")
	public ResponseEntity<AddCartResponseDto> addCart(@Valid @RequestBody AddCartDto addCartDto) throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType{
		AddCartResponseDto addCartResponseDto = cartService.addCart(addCartDto);
		return new ResponseEntity<>(addCartResponseDto, HttpStatus.OK);
	}

}
