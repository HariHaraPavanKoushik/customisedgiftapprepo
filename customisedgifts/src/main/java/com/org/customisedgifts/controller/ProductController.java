package com.org.customisedgifts.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.customisedgifts.entity.Product;
import com.org.customisedgifts.service.ProductService;

@RestController
@RequestMapping("Products")
public class ProductController {

	@Autowired
	ProductService productService;

	/**
	 * This method is used to check the list of products
	 * 
	 * @author uma
	 * @since 2020-01-04
	 * 
	 * @return ResponseEntity Object along with success message
	 * 
	 */
	@GetMapping("/getAllProducts")
	public ResponseEntity<List<Product>> getAllProducts() {
		List<Product> list = productService.getAllProducts();
		return new ResponseEntity<>(list, HttpStatus.OK);

	}

}
