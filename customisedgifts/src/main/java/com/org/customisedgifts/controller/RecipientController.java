/**
 * 
 */
package com.org.customisedgifts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.customisedgifts.dto.RecipientDto;
import com.org.customisedgifts.dto.ResponseDto;
import com.org.customisedgifts.service.RecipientService;

/**
 * @author ashan
 *
 */
@RestController
public class RecipientController {

	@Autowired
	RecipientService recipientService;

	@PutMapping("updateCartInformation")
	public ResponseEntity<ResponseDto> updateCartInformation(@RequestParam Long cardId,
			@RequestBody RecipientDto recipientDto) {
		ResponseDto responseDto = recipientService.updateCartInformation(cardId, recipientDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);

	}

}