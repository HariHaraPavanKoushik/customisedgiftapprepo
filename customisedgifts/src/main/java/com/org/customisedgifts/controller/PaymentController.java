package com.org.customisedgifts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.customisedgifts.dto.PaymentDto;
import com.org.customisedgifts.dto.PaymentResponse;
import com.org.customisedgifts.service.PaymentService;

@RestController
public class PaymentController {

	@Autowired
	PaymentService paymentService;

	@PutMapping("/pay/{cartId}")
	public ResponseEntity<PaymentResponse> pay(@PathVariable("cartId") long cartId,
			@RequestBody PaymentDto paymentDto) {
		PaymentResponse paymentResponse = paymentService.pay(cartId, paymentDto);
		return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
	}

}