package com.org.customisedgifts.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDto {
 
    String message;
    int statusCode;
    
    public ResponseDto(String message, int statusCode) {
        super();
        this.message = message;
        this.statusCode = statusCode;
    }
}
