package com.org.customisedgifts.dto;

import java.util.List;

import com.org.customisedgifts.entity.Recipient;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddCartDto {

	private long userId;
	private long productId;
	private String type;
	private int quantity;
	
	private List<Recipient> recipient;
}
