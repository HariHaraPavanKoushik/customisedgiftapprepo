package com.org.customisedgifts.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PaymentDto {

	private boolean status;

}
