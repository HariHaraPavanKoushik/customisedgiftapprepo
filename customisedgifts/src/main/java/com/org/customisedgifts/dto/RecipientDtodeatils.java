package com.org.customisedgifts.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RecipientDtodeatils {

	    
	    private long recipientId;
	    private String recipientName;
	    private String recipientEmailId;
	    private String message;
	    
}
