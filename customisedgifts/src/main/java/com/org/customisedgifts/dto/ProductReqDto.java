package com.org.customisedgifts.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductReqDto {

	private long productId;
	private String productName;
	private double productCost;
	private String productDesc;

}
