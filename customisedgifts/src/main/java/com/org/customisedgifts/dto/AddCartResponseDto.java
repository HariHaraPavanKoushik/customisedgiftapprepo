package com.org.customisedgifts.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddCartResponseDto {

	private String message;
	private int statusCode;
}
