package com.org.customisedgifts.constant;

public class UserUtility {

	public static final String USER_NOT_EXIST = "User does not EXIST";
	public static final int USER_NOT_EXIST_STATUS = 601;

	public static final String PRODUCT_NOT_FOUND = "Product does not Exist ";
	public static final int PRODUCT_NOT_FOUND_STATUS = 602;

	public static final String SUCCESSSFULLY_ADDED_TO_CART = "Succesfully added to cart";
	public static final int SUCCESSSFULLY_ADDED_TO_CART_STATUS = 603;
	
	public static final String QUANTITY_NOT_AVAILABLE = "Quantity not available";
	public static final int QUANTITY_NOT_AVAILABLE_STATUS = 604;
	
	public static final String NOT_PERSONAL = "Quantity not related to personal quantity";
	public static final int NOT_PERSONAL_STATUS = 605;
	
	public static final String NOT_CORPORATE = "Quantity not related to corporate quantity";
	public static final int NOT_CORPORATE_STATUS = 606;
	
	public static final String LIST_SIZE_NOT_CORRECT = "Recipient List size is not equal to quantity mentioned";
	public static final int LIST_SIZE_NOT_CORRECT_STATUS = 607;
	
	public static final String TYPE_NOT_MENTIONED = "Type of the purchase is not mentioned";
	public static final int TYPE_NOT_MENTIONED_STATUS = 607;
	
    public static final String NO_DATA_FOUND = "no data found";
    public static final int NO_DATA_FOUND_STATUS = 654;
   
    public static final String CANNOT_UPDATE = "cannot update because your order confirmed";
    public static final int CANNOT_UPDATE_STATUSCODE = 994;
    
    public static final String UPDATE_DONE = "Successfully updated";
    public static final int UPDATE_STATUSCODE = 676;
	
	
	private UserUtility() {

	}

}
