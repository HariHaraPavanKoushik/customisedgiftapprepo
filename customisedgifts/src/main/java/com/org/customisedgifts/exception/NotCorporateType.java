package com.org.customisedgifts.exception;

public class NotCorporateType extends Exception{

	private static final long serialVersionUID = 1L;

	public NotCorporateType(String message) {
		super(message);
	}
}
