package com.org.customisedgifts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.org.customisedgifts.constant.UserUtility;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(CustomException.class)
	public ResponseEntity<ErrorResponse> customException(CustomException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.USER_NOT_EXIST_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(ProductNotFound.class)
	public ResponseEntity<ErrorResponse> productNotFound(ProductNotFound ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.PRODUCT_NOT_FOUND_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(QuantityNotAvailable.class)
	public ResponseEntity<ErrorResponse> quantityNotAvailable(QuantityNotAvailable ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.QUANTITY_NOT_AVAILABLE_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(NotPersonalType.class)
	public ResponseEntity<ErrorResponse> notPersonalType(NotPersonalType ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.NOT_PERSONAL_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(NotCorporateType.class)
	public ResponseEntity<ErrorResponse> notCorporateType(NotCorporateType ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.NOT_CORPORATE_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(PaymentException.class)
	public ResponseEntity<ErrorStatus> paymentException(PaymentException ex) {

		ErrorStatus errorStatus = new ErrorStatus();

		errorStatus.setStatuscode(700);
		errorStatus.setStatusmessage("Already paid and mail sent");
		return new ResponseEntity<>(errorStatus, HttpStatus.BAD_REQUEST);

	}

}