package com.org.customisedgifts.exception;

public class NotPersonalType extends Exception{

	private static final long serialVersionUID = 1L;

	public NotPersonalType(String message) {
		super(message);
	}
}
