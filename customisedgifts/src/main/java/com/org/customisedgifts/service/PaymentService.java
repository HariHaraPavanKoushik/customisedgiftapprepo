package com.org.customisedgifts.service;

import com.org.customisedgifts.dto.PaymentDto;
import com.org.customisedgifts.dto.PaymentResponse;

public interface PaymentService {

	PaymentResponse pay(long cartId, PaymentDto paymentDto);

}
