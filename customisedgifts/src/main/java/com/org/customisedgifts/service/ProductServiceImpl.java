package com.org.customisedgifts.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.customisedgifts.entity.Product;
import com.org.customisedgifts.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	public List<Product> getAllProducts() {

		List<Product> product = null ;
			product =	productRepository.findAll();

		return product;

	}

}
