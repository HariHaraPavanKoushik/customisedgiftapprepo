package com.org.customisedgifts.service;

import com.org.customisedgifts.dto.RecipientDto;
import com.org.customisedgifts.dto.ResponseDto;

public interface RecipientService {

	ResponseDto updateCartInformation(Long cardId, RecipientDto recipientDto);

}