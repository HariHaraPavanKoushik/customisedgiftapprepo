package com.org.customisedgifts.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.customisedgifts.dto.PaymentDto;
import com.org.customisedgifts.dto.PaymentResponse;
import com.org.customisedgifts.entity.Cart;
import com.org.customisedgifts.entity.Order;
import com.org.customisedgifts.entity.Recipient;
import com.org.customisedgifts.exception.PaymentException;
import com.org.customisedgifts.repository.CartRepository;
import com.org.customisedgifts.repository.OrderRepository;
import com.org.customisedgifts.repository.RecipientRepository;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	CartRepository cartRepository;

	@Autowired
	MailServiceImpl mailServiceImpl;

	@Autowired
	RecipientRepository recipientRepository;

	@Autowired
	OrderRepository orderRepository;

	@Override
	public PaymentResponse pay(long cartId, PaymentDto paymentDto) {

		boolean status = cartRepository.fetchStatus(cartId);
		if (!status) {
			cartRepository.updateStatus(cartId, paymentDto.isStatus());

			Order order = new Order();
			Cart cart = cartRepository.findByCartId(cartId);
			order.setCart(cart);
			order.setPurchasedate(LocalDate.now());

			orderRepository.save(order);
			List<Recipient> list = recipientRepository.findByCartCartId(cartId);

			list.forEach(entity -> 
				mailServiceImpl.sendMail(entity.getRecipientEmailId(), entity.getCart().getUser().getEmailId(),
						entity.getMessage(),entity.getRecipientName())
			);
			return new PaymentResponse("Mail Sent Successfully", 101);
		} else {
			throw new PaymentException("Already booked and mail sent", 700);
		}
	}
}