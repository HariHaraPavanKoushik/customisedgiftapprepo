package com.org.customisedgifts.service;

import java.util.List;

import com.org.customisedgifts.entity.Product;

public interface ProductService {

	public List<Product> getAllProducts();

}
