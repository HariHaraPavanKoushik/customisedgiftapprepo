package com.org.customisedgifts.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.customisedgifts.constant.UserUtility;
import com.org.customisedgifts.dto.AddCartDto;
import com.org.customisedgifts.dto.AddCartResponseDto;
import com.org.customisedgifts.entity.Cart;
import com.org.customisedgifts.entity.Product;
import com.org.customisedgifts.entity.Recipient;
import com.org.customisedgifts.entity.User;
import com.org.customisedgifts.exception.CustomException;
import com.org.customisedgifts.exception.NotCorporateType;
import com.org.customisedgifts.exception.NotPersonalType;
import com.org.customisedgifts.exception.ProductNotFound;
import com.org.customisedgifts.exception.QuantityNotAvailable;
import com.org.customisedgifts.repository.CartRepository;
import com.org.customisedgifts.repository.ProductRepository;
import com.org.customisedgifts.repository.RecipientRepository;
import com.org.customisedgifts.repository.UserRepository;

@Service
public class CartServiceImpl implements CartService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	CartRepository cartRepository;

	@Autowired
	RecipientRepository recipientRepository;

	/**
	 * In this functionality we will add the details to the cart
	 * @param addCartDto
	 * @return
	 * @throws CustomException		-for user not found exception
	 * @throws ProductNotFound		-for product not found exception
	 * @throws QuantityNotAvailable	-for quantity not available 
	 * @throws NotPersonalType		-for product type checking not personal
	 * @throws NotCorporateType		-for product type checking not corporate
	 * 
	 */
	@Override
	public AddCartResponseDto addCart(@Valid AddCartDto addCartDto)
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		List<Recipient> list =null;
		User user = userRepository.findByUserId(addCartDto.getUserId());
		Cart cart = new Cart();
		if (user == null) {
			throw new CustomException(UserUtility.USER_NOT_EXIST);
		}

		Product product = productRepository.findByProductId(addCartDto.getProductId());
		if (product == null) {
			throw new ProductNotFound(UserUtility.PRODUCT_NOT_FOUND);
		}

		if (addCartDto.getQuantity() > product.getQuantity()) {
			throw new QuantityNotAvailable(UserUtility.QUANTITY_NOT_AVAILABLE);
		}

		if (addCartDto.getType().equals("personal")) {
			if (addCartDto.getQuantity() > 3) {
				throw new NotPersonalType(UserUtility.NOT_PERSONAL);
			}
			
			public AddCartResponseDto validation() {
			list = addCartDto.getRecipient();
			
			if (list.size() != addCartDto.getQuantity()) {
				AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
				addCartResponseDto.setMessage(UserUtility.LIST_SIZE_NOT_CORRECT);
				addCartResponseDto.setStatusCode(UserUtility.LIST_SIZE_NOT_CORRECT_STATUS);
				return addCartResponseDto;
			}
			cart.setUser(user);
			cart.setProduct(product);

			double price = product.getProductCost() * addCartDto.getQuantity();
			cart.setPrice(price);
			BeanUtils.copyProperties(addCartDto, cart);
			product.setQuantity(product.getQuantity()-addCartDto.getQuantity());
			cart.setStatus(false);
			Cart cartRepo = cartRepository.save(cart);
			
			  List<Recipient> recipientlist1 = new ArrayList<>();
			addCartDto.getRecipient().forEach(entity -> {
				Recipient recipient = new Recipient();
				recipient.setRecipientEmailId(entity.getRecipientEmailId());
				recipient.setRecipientName(entity.getRecipientName());
				recipient.setMessage(entity.getMessage());
				recipient.setCart(cartRepo);
				 BeanUtils.copyProperties(addCartDto.getRecipient(), recipientlist1);
					recipientlist1.add(recipient);
				
			});
			
			  recipientRepository.saveAll(recipientlist1);
			
			AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
			addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
			addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);
			
			return addCartResponseDto;
			}


		}else if (addCartDto.getType().equals("corporate")) {
			if (addCartDto.getQuantity() < 5) {
				throw new NotCorporateType(UserUtility.NOT_CORPORATE);
			}
			list = addCartDto.getRecipient();
			if (list.size() != addCartDto.getQuantity()) {
				AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
				addCartResponseDto.setMessage(UserUtility.LIST_SIZE_NOT_CORRECT);
				addCartResponseDto.setStatusCode(UserUtility.LIST_SIZE_NOT_CORRECT_STATUS);
				return addCartResponseDto;
			}
			cart.setUser(user);
			cart.setProduct(product);

			double price = product.getProductCost() * addCartDto.getQuantity();
			cart.setPrice(price);
			BeanUtils.copyProperties(addCartDto, cart);
			product.setQuantity(product.getQuantity()-addCartDto.getQuantity());
			cart.setStatus(false);
			Cart cartRepo = cartRepository.save(cart);
			
			  List<Recipient> recipientlist1 = new ArrayList<>();
			addCartDto.getRecipient().forEach(entity -> {
				Recipient recipient = new Recipient();
				recipient.setRecipientEmailId(entity.getRecipientEmailId());
				recipient.setRecipientName(entity.getRecipientName());
				recipient.setMessage(entity.getMessage());
				recipient.setCart(cartRepo);
				 BeanUtils.copyProperties(addCartDto.getRecipient(), recipientlist1);
					recipientlist1.add(recipient);
				
			});
			
			  recipientRepository.saveAll(recipientlist1);
			
			AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
			addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
			addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);

			return addCartResponseDto;
		}
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.TYPE_NOT_MENTIONED);
		addCartResponseDto.setStatusCode(UserUtility.TYPE_NOT_MENTIONED_STATUS);

		return addCartResponseDto;
	}

}
