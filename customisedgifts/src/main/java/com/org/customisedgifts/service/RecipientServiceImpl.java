package com.org.customisedgifts.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.customisedgifts.constant.UserUtility;
import com.org.customisedgifts.dto.RecipientDto;
import com.org.customisedgifts.dto.ResponseDto;
import com.org.customisedgifts.entity.Recipient;
import com.org.customisedgifts.repository.CartRepository;
import com.org.customisedgifts.repository.RecipientRepository;

/**
 * @author asHan
 *
 */
@Service
public class RecipientServiceImpl implements RecipientService {

	@Autowired
	CartRepository cartRepository;

	@Autowired
	RecipientRepository recipientRepository;

	@SuppressWarnings("unused")
	@Override
	public ResponseDto updateCartInformation(Long cardId, RecipientDto recipientDto) {

		Recipient recipient = new Recipient();
		BeanUtils.copyProperties(recipientDto, recipient);
		if (recipient != null) {

			Boolean status = cartRepository.fetchbyCartStatus(cardId);
System.out.println(status);
			if (!status) {
				System.out.println(status);
				recipientDto.getRecipientDtodetails().forEach(entity -> {
					Recipient recipient1 = recipientRepository.fetchById(entity.getRecipientId());

					if (!entity.getRecipientEmailId().isEmpty()) {
						recipient1.setRecipientEmailId(entity.getRecipientEmailId());
					}
					if (!entity.getMessage().isEmpty()) {
						recipient1.setMessage(entity.getMessage());
					}
					if (!entity.getRecipientName().isEmpty()) {
						recipient1.setRecipientName(entity.getRecipientName());
					}
					recipientRepository.save(recipient1);

				});
				return new ResponseDto(UserUtility.UPDATE_DONE, UserUtility.UPDATE_STATUSCODE);
			}
			return new ResponseDto(UserUtility.CANNOT_UPDATE, UserUtility.CANNOT_UPDATE_STATUSCODE);

		}
		return new ResponseDto(UserUtility.NO_DATA_FOUND, UserUtility.NO_DATA_FOUND_STATUS);
	}

}