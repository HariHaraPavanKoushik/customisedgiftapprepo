package com.org.customisedgifts.service;

import javax.validation.Valid;

import com.org.customisedgifts.dto.AddCartDto;
import com.org.customisedgifts.dto.AddCartResponseDto;
import com.org.customisedgifts.exception.CustomException;
import com.org.customisedgifts.exception.NotCorporateType;
import com.org.customisedgifts.exception.NotPersonalType;
import com.org.customisedgifts.exception.ProductNotFound;
import com.org.customisedgifts.exception.QuantityNotAvailable;

public interface CartService {

	AddCartResponseDto addCart(@Valid AddCartDto addCartDto) throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType;

}
