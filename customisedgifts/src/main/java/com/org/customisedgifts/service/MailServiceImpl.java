package com.org.customisedgifts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailServiceImpl {

	@Autowired
	JavaMailSender javaMailSender;

	public void sendMail(String to, String from, String message,String recipientName) {
		try {
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(to);
			msg.setFrom(from);
			msg.setCc(from);
			msg.setSubject("Congratulations !!");
			msg.setText("Dear"+" "+recipientName +'\n'+message);
			javaMailSender.send(msg);
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
