package com.org.customisedgifts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomisedgiftsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomisedgiftsApplication.class, args);
	}

}
