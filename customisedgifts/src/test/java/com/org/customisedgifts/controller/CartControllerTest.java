package com.org.customisedgifts.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.org.customisedgifts.constant.UserUtility;
import com.org.customisedgifts.dto.AddCartDto;
import com.org.customisedgifts.dto.AddCartResponseDto;
import com.org.customisedgifts.exception.CustomException;
import com.org.customisedgifts.exception.NotCorporateType;
import com.org.customisedgifts.exception.NotPersonalType;
import com.org.customisedgifts.exception.ProductNotFound;
import com.org.customisedgifts.exception.QuantityNotAvailable;
import com.org.customisedgifts.service.CartService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CartControllerTest {
	@InjectMocks
	CartController cartController;
	@Mock
	CartService cartService;

	@Test
	public void addCartTest() throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(2);
		addCartDto.setUserId(1233);
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);
		Mockito.when(cartService.addCart(addCartDto)).thenReturn(addCartResponseDto);
		ResponseEntity<AddCartResponseDto> result = cartController.addCart(addCartDto);
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

}