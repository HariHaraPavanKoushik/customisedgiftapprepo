package com.org.customisedgifts.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.org.customisedgifts.constant.UserUtility;
import com.org.customisedgifts.dto.RecipientDto;
import com.org.customisedgifts.dto.ResponseDto;
import com.org.customisedgifts.exception.CustomException;
import com.org.customisedgifts.service.RecipientService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RecipientControllerTest {
	@InjectMocks
	RecipientController recipientController;
	@Mock
	RecipientService recipientService;

	@Test
	public void updateCartInformationTest() throws CustomException {
		RecipientDto recipientDto = new RecipientDto();

		ResponseDto responseDto = new ResponseDto(UserUtility.UPDATE_DONE, UserUtility.UPDATE_STATUSCODE);

		responseDto.setMessage(UserUtility.UPDATE_DONE);
		responseDto.setStatusCode(UserUtility.UPDATE_STATUSCODE);

		Mockito.when(recipientService.updateCartInformation(1L, recipientDto)).thenReturn(responseDto);

		ResponseEntity<ResponseDto> result = recipientController.updateCartInformation(1L, recipientDto);
		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

}
