package com.org.customisedgifts.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.org.customisedgifts.entity.Product;
import com.org.customisedgifts.service.ProductService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductControllerTest {

	@InjectMocks
	ProductController productController;
	@Mock
	ProductService productService;

	@Test
	public void getAllProductsTest() {
		Product product = new Product();

		List<Product> list = new ArrayList<>();
		list.add(product);
		Mockito.when(productService.getAllProducts()).thenReturn(list);
		ResponseEntity<List<Product>> result = productController.getAllProducts();
		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

}