package com.org.customisedgifts.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.org.customisedgifts.dto.PaymentDto;
import com.org.customisedgifts.dto.PaymentResponse;
import com.org.customisedgifts.service.PaymentService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PaymentControllerTest {

	@Mock
	PaymentService paymentService;

	@InjectMocks
	PaymentController paymentController;

	@Test
	public void testPay() {
		PaymentResponse paymentResponse = new PaymentResponse("Success", 200);
		PaymentDto paymentDto = new PaymentDto();

		Mockito.when(paymentService.pay(1l, paymentDto)).thenReturn(paymentResponse);
		ResponseEntity<PaymentResponse> result = paymentController.pay(1l, paymentDto);
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

}
