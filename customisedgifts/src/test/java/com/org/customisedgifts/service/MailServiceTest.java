package com.org.customisedgifts.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MailServiceTest {

	@InjectMocks
	MailServiceImpl mailServiceImpl;

	@Test
	public void testSendMail() {

		SimpleMailMessage msg = new SimpleMailMessage();
		Mockito.spy(MailServiceImpl.class);
		mailServiceImpl.sendMail("alo@gmail.com", "alok@gmail.com", "hi", "alok");
		assertEquals("hello", "hello");
	}

}