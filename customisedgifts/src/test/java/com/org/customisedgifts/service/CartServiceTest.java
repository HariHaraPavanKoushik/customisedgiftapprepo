package com.org.customisedgifts.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.customisedgifts.constant.UserUtility;
import com.org.customisedgifts.dto.AddCartDto;
import com.org.customisedgifts.dto.AddCartResponseDto;
import com.org.customisedgifts.entity.Cart;
import com.org.customisedgifts.entity.Product;
import com.org.customisedgifts.entity.Recipient;
import com.org.customisedgifts.entity.User;
import com.org.customisedgifts.exception.CustomException;
import com.org.customisedgifts.exception.NotCorporateType;
import com.org.customisedgifts.exception.NotPersonalType;
import com.org.customisedgifts.exception.ProductNotFound;
import com.org.customisedgifts.exception.QuantityNotAvailable;
import com.org.customisedgifts.repository.CartRepository;
import com.org.customisedgifts.repository.ProductRepository;
import com.org.customisedgifts.repository.RecipientRepository;
import com.org.customisedgifts.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CartServiceTest {
	@InjectMocks
	CartServiceImpl cartServiceImpl;
	@Mock
	CartRepository cartRepository;
	@Mock
	UserRepository userRepository;
	@Mock
	ProductRepository productRepository;
	@Mock
	RecipientRepository recipientRepository;

	@Test
	public void addCartTest()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);
		User user = new User();
		Product product = new Product();
		product.setQuantity(10);
		product.setProductCost(2000);
	
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setUser(user);
		cart.setProduct(product);
		cart.setStatus(false);
		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(6);
		addCartDto.setProductId(1234);
		addCartDto.setUserId(1);
		addCartDto.setType("corporate");
		Recipient recipient = new Recipient();
		recipient.setMessage("hekk");
		recipient.setCart(cart);
		recipient.setRecipientEmailId("dfgh");
		recipient.setRecipientId(1);
		recipient.setRecipientName("name");
		Recipient reci1 = new Recipient();
		reci1.setMessage("hekk");
		reci1.setCart(cart);
		reci1.setRecipientEmailId("dfgh");
		reci1.setRecipientId(1);
		reci1.setRecipientName("name");

		List<Recipient> ls = new ArrayList<>();
		ls.add(recipient);
		ls.add(reci1);
		addCartDto.setRecipient(ls);
		
		
		
		
		recipient = new Recipient();
		recipient.setMessage("hekk");
		recipient.setCart(cart);
		recipient.setRecipientEmailId("dfgh");
		recipient.setRecipientId(1);
		recipient.setRecipientName("name");
		reci1 = new Recipient();
		reci1.setMessage("hekk");
		reci1.setCart(cart);
		reci1.setRecipientEmailId("dfgh");
		reci1.setRecipientId(1);
		reci1.setRecipientName("name");
		
		ls = new ArrayList<>();
		ls.add(recipient);
		ls.add(reci1);
		System.out.println(ls.size());
		System.out.println(addCartDto.getQuantity());
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(user);
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(product);
		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		Mockito.when( recipientRepository.saveAll(ls)).thenReturn(ls);
		
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(607, result.getStatusCode());
	}
	
	@Test
	public void addCartTest1()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(1);
		addCartDto.setProductId(1234);
		addCartDto.setUserId(1);
		addCartDto.setType("personal");
		Recipient recipient = new Recipient();
		recipient.setMessage("hekk");
		recipient.setCart(cart);
		recipient.setRecipientEmailId("dfgh");
		recipient.setRecipientId(1);
		recipient.setRecipientName("name");
		Recipient reci1 = new Recipient();
		reci1.setMessage("hekk");
		reci1.setCart(cart);
		reci1.setRecipientEmailId("dfgh");
		reci1.setRecipientId(1);
		reci1.setRecipientName("name");

		List<Recipient> ls = new ArrayList<>();
		ls.add(recipient);
		ls.add(reci1);
		addCartDto.setRecipient(ls);
		
		User user = new User();
		Product product = new Product();
		product.setQuantity(10);
		
		recipient = new Recipient();
		recipient.setMessage("hekk");
		recipient.setCart(cart);
		recipient.setRecipientEmailId("dfgh");
		recipient.setRecipientId(1);
		recipient.setRecipientName("name");
		reci1 = new Recipient();
		reci1.setMessage("hekk");
		reci1.setCart(cart);
		reci1.setRecipientEmailId("dfgh");
		reci1.setRecipientId(1);
		reci1.setRecipientName("name");
		
		ls = new ArrayList<>();
		ls.add(recipient);
		ls.add(reci1);
		System.out.println(ls.size());
		System.out.println(addCartDto.getQuantity());
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(user);
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(product);
		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		Mockito.when( recipientRepository.saveAll(ls)).thenReturn(ls);
		
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(607, result.getStatusCode());
	}
	
	@Test
	public void addCartTest2()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);
		User user = new User();
		Product product = new Product();
		product.setQuantity(10);
		product.setProductCost(2000);
	
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setUser(user);
		cart.setProduct(product);
		cart.setStatus(false);
		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(6);
		addCartDto.setProductId(1234);
		addCartDto.setUserId(1);
		addCartDto.setType("corporate");
		Recipient recipient = new Recipient();

		Recipient reci1 = new Recipient();
		Recipient reci2 = new Recipient();
		Recipient reci3 = new Recipient();
		Recipient reci4 = new Recipient();
		Recipient reci5 = new Recipient();
	

		List<Recipient> ls = new ArrayList<>();
		ls.add(recipient);
		ls.add(reci1);
		ls.add(reci5);
		ls.add(reci4);
		ls.add(reci3);
		ls.add(reci2);
		addCartDto.setRecipient(ls);
		
		
		
		
		recipient = new Recipient();
		recipient.setMessage("hekk");
		recipient.setCart(cart);
		recipient.setRecipientEmailId("dfgh");
		recipient.setRecipientId(1);
		recipient.setRecipientName("name");
		reci1 = new Recipient();
		reci1.setMessage("hekk");
		reci1.setCart(cart);
		reci1.setRecipientEmailId("dfgh");
		reci1.setRecipientId(1);
		reci1.setRecipientName("name");
		
		ls = new ArrayList<>();
		ls.add(recipient);
		ls.add(reci1);
		System.out.println(ls.size());
		System.out.println(addCartDto.getQuantity());
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(user);
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(product);
		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		Mockito.when( recipientRepository.saveAll(ls)).thenReturn(ls);
		
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(603, result.getStatusCode());
	}
	
	@Test
	public void addCartTest3()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);
		User user = new User();
		Product product = new Product();
		product.setQuantity(10);
		product.setProductCost(2000);
	
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setUser(user);
		cart.setProduct(product);
		cart.setStatus(false);
		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(2);
		addCartDto.setProductId(1234);
		addCartDto.setUserId(1);
		addCartDto.setType("personal");
		Recipient recipient = new Recipient();
		recipient.setMessage("hekk");
		recipient.setCart(cart);
		recipient.setRecipientEmailId("dfgh");
		recipient.setRecipientId(1);
		recipient.setRecipientName("name");
		Recipient reci1 = new Recipient();
		reci1.setMessage("hekk");
		reci1.setCart(cart);
		reci1.setRecipientEmailId("dfgh");
		reci1.setRecipientId(1);
		reci1.setRecipientName("name");

		List<Recipient> ls = new ArrayList<>();
		ls.add(recipient);
		ls.add(reci1);
		addCartDto.setRecipient(ls);
		
		
		
		
		recipient = new Recipient();
		recipient.setMessage("hekk");
		recipient.setCart(cart);
		recipient.setRecipientEmailId("dfgh");
		recipient.setRecipientId(1);
		recipient.setRecipientName("name");
		reci1 = new Recipient();
		reci1.setMessage("hekk");
		reci1.setCart(cart);
		reci1.setRecipientEmailId("dfgh");
		reci1.setRecipientId(1);
		reci1.setRecipientName("name");
		
		ls = new ArrayList<>();
		ls.add(recipient);
		ls.add(reci1);
		System.out.println(ls.size());
		System.out.println(addCartDto.getQuantity());
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(user);
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(product);
		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		Mockito.when( recipientRepository.saveAll(ls)).thenReturn(ls);
		
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(603, result.getStatusCode());
	}
	
	@Test(expected = CustomException.class)
	public void addCartTestNegative() throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);

		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(1);
		addCartDto.setProductId(1);
		addCartDto.setUserId(0);
		addCartDto.setType("personal");
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setQuantity(4);
		cart.setType("personal");
		
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(null);

		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(UserUtility.SUCCESSSFULLY_ADDED_TO_CART, result.getStatusCode());
	}
	
	@Test(expected = CustomException.class)
	public void addCartTestNegative1()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);

		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(1);
		addCartDto.setProductId(0);
		addCartDto.setUserId(0);
		addCartDto.setType("personal");
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setQuantity(4);
		cart.setType("personal");
		
		
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(null);

		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(UserUtility.PRODUCT_NOT_FOUND_STATUS, result.getStatusCode());
	}
	
	@Test(expected = ProductNotFound.class)
	public void addCartTestNegative2()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);

		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(1);
		addCartDto.setProductId(0);
		addCartDto.setUserId(0);
		addCartDto.setType("personal");
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setQuantity(4);
		cart.setType("personal");
		User user = new User();
		user.setEmailId("email");
		
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(user);
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(null);

		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(UserUtility.PRODUCT_NOT_FOUND_STATUS, result.getStatusCode());
	}
	
	@Test(expected = QuantityNotAvailable.class)
	public void addCartTestNegative3()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);

		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(10);
		addCartDto.setProductId(0);
		addCartDto.setUserId(0);
		addCartDto.setType("personal");
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setQuantity(4);
		cart.setType("personal");
		User user = new User();
		user.setEmailId("email");
		Product product = new Product();
		product.setProductName("pd2");
		product.setQuantity(3);
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(user);
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(product);

		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(UserUtility.PRODUCT_NOT_FOUND_STATUS, result.getStatusCode());
	}
	
	@Test(expected = NotPersonalType.class)
	public void addCartTestNegative4()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);

		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(5);
		addCartDto.setProductId(0);
		addCartDto.setUserId(0);
		addCartDto.setType("personal");
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setQuantity(4);
		cart.setType("personal");
		User user = new User();
		user.setEmailId("email");
		Product product = new Product();
		product.setProductName("pd2");
		product.setQuantity(10);
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(user);
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(product);

		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(UserUtility.PRODUCT_NOT_FOUND_STATUS, result.getStatusCode());
	}
	
	@Test(expected = NotCorporateType.class)
	public void addCartTestNegative5()
			throws CustomException, ProductNotFound, QuantityNotAvailable, NotPersonalType, NotCorporateType {
		AddCartResponseDto addCartResponseDto = new AddCartResponseDto();
		addCartResponseDto.setMessage(UserUtility.SUCCESSSFULLY_ADDED_TO_CART);
		addCartResponseDto.setStatusCode(UserUtility.SUCCESSSFULLY_ADDED_TO_CART_STATUS);

		AddCartDto addCartDto = new AddCartDto();
		addCartDto.setQuantity(4);
		addCartDto.setProductId(0);
		addCartDto.setUserId(0);
		addCartDto.setType("corporate");
		Cart cart = new Cart();
		cart.setCartId(1234);
		cart.setPrice(2000);
		cart.setQuantity(4);
		cart.setType("personal");
		User user = new User();
		user.setEmailId("email");
		Product product = new Product();
		product.setProductName("pd2");
		product.setQuantity(10);
		Mockito.when(userRepository.findByUserId(addCartDto.getUserId())).thenReturn(user);
		Mockito.when(productRepository.findByProductId(addCartDto.getProductId())).thenReturn(product);

		Mockito.when(cartRepository.save(cart)).thenReturn(cart);
		AddCartResponseDto result = cartServiceImpl.addCart(addCartDto);
		assertEquals(UserUtility.PRODUCT_NOT_FOUND_STATUS, result.getStatusCode());
	}
}
