package com.org.customisedgifts.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.customisedgifts.entity.Product;
import com.org.customisedgifts.repository.ProductRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductServiceTest {
	@InjectMocks
	ProductServiceImpl productServiceImpl;
	@Mock
	ProductRepository productRepository;

	@Test
	public void getAllProductsTest() {
		Product product = new Product();

		List<Product> list = new ArrayList<>();
		list.add(product);
		Mockito.when(productRepository.findAll()).thenReturn(list);
		List<Product> productlst = productServiceImpl.getAllProducts();
		assertEquals(1, productlst.size());
	}

}
