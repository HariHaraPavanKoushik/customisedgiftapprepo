package com.org.customisedgifts.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.customisedgifts.dto.RecipientDto;
import com.org.customisedgifts.dto.RecipientDtodeatils;
import com.org.customisedgifts.dto.ResponseDto;
import com.org.customisedgifts.entity.Cart;
import com.org.customisedgifts.entity.Recipient;
import com.org.customisedgifts.repository.CartRepository;
import com.org.customisedgifts.repository.RecipientRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RecipientServiceTest {
	@InjectMocks
	RecipientServiceImpl recipientServiceImpl;
	@Mock
	RecipientRepository recipientRepository;
	@Mock
	CartRepository cartRepository;

	@Test
	public void updateCartInformationTest() {
		RecipientDto recipientDto = new RecipientDto();
		List<RecipientDtodeatils> recipientDtos = new ArrayList<>();
		
		
		recipientDto.setRecipientDtodetails(recipientDtos);
	
		Recipient recipient = new Recipient();
		recipient.setRecipientId(1234);
		recipient.setRecipientEmailId("abc@hcl.com");
		List<Recipient> recipientls = new ArrayList<>();
		recipientls.add(recipient);
		
		Mockito.when(cartRepository.fetchbyCartStatus(Mockito.anyLong())).thenReturn(true);
		Mockito.when(recipientRepository.fetchById(Mockito.anyLong())).thenReturn(recipient);
		Mockito.when(recipientRepository.save(recipient)).thenReturn(recipient);
		
		ResponseDto result = recipientServiceImpl.updateCartInformation(1L, recipientDto);
		assertEquals(994, result.getStatusCode());

	}
	
	@Test
	public void UpdateCartInformationTest1() {
		Cart cart = new Cart();
		RecipientDto recipientDto = new RecipientDto();
		List<RecipientDtodeatils> recipientDtos = new ArrayList<>();
		
		recipientDto.setRecipientDtodetails(recipientDtos);
	
		Recipient recipient = null;

		Mockito.when(cartRepository.fetchbyCartStatus(Mockito.anyLong())).thenReturn(false);
		Recipient recipient2 = new Recipient();
		recipient2.setMessage("message");
		recipient2.setRecipientEmailId(null);
		recipient2.setRecipientId(1);
		recipient2.setRecipientName("name");
		recipient2.setCart(cart);
		Mockito.when(recipientRepository.fetchById(Mockito.anyLong())).thenReturn(recipient2);
		
		
		Mockito.when(recipientRepository.save(recipient)).thenReturn(recipient);
		
		ResponseDto result = recipientServiceImpl.updateCartInformation(1L, recipientDto);
		assertEquals(676, result.getStatusCode());

	}
	
	@Test
	public void UpdateCartInformationTest2() {
		
		RecipientDto recipientDto = new RecipientDto();
		List<RecipientDtodeatils> recipientDtos = new ArrayList<>();
		
		recipientDto.setRecipientDtodetails(recipientDtos);
	
		Recipient recipient =null;
		
		Mockito.when(cartRepository.fetchbyCartStatus(Mockito.anyLong())).thenReturn(false);
		
		
	
//		Mockito.when(recipientRepository.fetchById(Mockito.anyLong())).thenReturn(null);
		
		
		Mockito.when(recipientRepository.save(recipient)).thenReturn(recipient);
		
		ResponseDto result = recipientServiceImpl.updateCartInformation(1L, recipientDto);
		assertEquals(676, result.getStatusCode());

	}
}
