package com.org.customisedgifts.service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.customisedgifts.dto.PaymentDto;
import com.org.customisedgifts.dto.PaymentResponse;
import com.org.customisedgifts.entity.Order;
import com.org.customisedgifts.entity.Recipient;
import com.org.customisedgifts.exception.PaymentException;
import com.org.customisedgifts.repository.CartRepository;
import com.org.customisedgifts.repository.OrderRepository;
import com.org.customisedgifts.repository.RecipientRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PaymentServiceImplTest {

	@Mock
	CartRepository cartRepository;

	@Mock
	RecipientRepository recipientRepository;

	@Mock
	MailServiceImpl mailServiceImpl;
	
	@Mock
	OrderRepository orderRepository;

	@InjectMocks
	PaymentServiceImpl paymentServiceImpl;

	@Test
	public void testpay() {
		PaymentDto paymentDto = new PaymentDto();
		paymentDto.setStatus(false);
		Recipient r = new Recipient();
		Order order = new Order();
		order.setPurchasedate(LocalDate.now());
		List<Recipient> ls = new ArrayList<>();
		Mockito.when(orderRepository.save(order)).thenReturn(order);
		Mockito.when(recipientRepository.findByCartCartId(Mockito.anyLong())).thenReturn(ls);
		Mockito.spy(PaymentServiceImpl.class);
		PaymentResponse result = paymentServiceImpl.pay(1l, paymentDto);
		assertEquals(101, result.getStatusCode());
	}
	
	@Test(expected = PaymentException.class)
	public void testpay1() {
		PaymentDto paymentDto = new PaymentDto();
		paymentDto.setStatus(true);
		Recipient r = new Recipient();
		Order order = new Order();
		order.setPurchasedate(LocalDate.now());
		List<Recipient> ls = new ArrayList<>();
		Mockito.when(cartRepository.fetchStatus(Mockito.anyLong())).thenReturn(true);
		Mockito.when(orderRepository.save(order)).thenReturn(order);
		Mockito.when(recipientRepository.findByCartCartId(Mockito.anyLong())).thenReturn(ls);
		Mockito.spy(PaymentServiceImpl.class);
		PaymentResponse result = paymentServiceImpl.pay(1l, paymentDto);
		assertEquals(101, result.getStatusCode());
	}

}
